package com.example.deltech.Util;

import android.text.TextUtils;
import android.util.Patterns;

public class Util {
    public static String app_database="app_database";
    public static String student="student";

    public static boolean isValidEmail(String target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }
}
