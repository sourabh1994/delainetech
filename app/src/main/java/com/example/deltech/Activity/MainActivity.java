package com.example.deltech.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.example.deltech.Fragment.FirstFrag;
import com.example.deltech.R;

public class MainActivity extends AppCompatActivity {

    FragmentManager fm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fm=getSupportFragmentManager();
        switchFragment(new FirstFrag(),0,"firstFrag");
    }
    public void switchFragment(Fragment fragment, int position, String fragName) {
        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        if (position == 0) {
            fragmentTransaction.add(R.id.container, fragment, fragName);
        } else{
            fragmentTransaction.replace(R.id.container, fragment, fragName);
            fragmentTransaction.addToBackStack(fragName);
        }
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
