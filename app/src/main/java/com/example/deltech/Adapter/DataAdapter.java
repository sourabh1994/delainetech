package com.example.deltech.Adapter;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import androidx.appcompat.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.deltech.Interface.DataListenerInterface;
import com.example.deltech.Pojo.DataPojo;
import com.example.deltech.Activity.MainActivity;
import com.example.deltech.R;
import com.example.deltech.Fragment.SecondFrag;

import java.util.List;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.MyViewHolder> {
    @NonNull
    AlertDialog.Builder builder;
    private List<DataPojo> dataList;
    Context context;
    private String[] options;
    AppDbAdapter appDbAdapter;
    DataListenerInterface dataListenerInterface;
    public DataAdapter(@NonNull List<DataPojo> dataList,Context context, DataListenerInterface dataListenerInterface) {
        this.dataList = dataList;
        this.context=context;
        this.dataListenerInterface=dataListenerInterface;
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recyclerdata, viewGroup, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder myViewHolder, int i) {
        DataPojo data = dataList.get(i);
        myViewHolder.name.setText(data.getName());
        myViewHolder.email.setText(data.getEmail());
        myViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                options = context.getResources().getStringArray(R.array.options);
                appDbAdapter=new AppDbAdapter(context);
                builder = new AlertDialog.Builder(context);
                builder.setTitle("Select Action");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (options[i].equals("Update Record")) {
                            Bundle bundle =new Bundle();
                            bundle.putString("email",myViewHolder.email.getText().toString());
                            bundle.putString("name",myViewHolder.name.getText().toString());
                            bundle.putString("button","update");
                            SecondFrag secondFrag=new SecondFrag();
                            secondFrag.setArguments(bundle);
                            ((MainActivity) context).switchFragment(secondFrag, 1, "secondFrag");
                        } else if (options[i].equals("Delete Record")) {
                            appDbAdapter.deleteData(myViewHolder.email.getText().toString());
                            dataListenerInterface.data(true);
                        } else if (options[i].equals("Cancel")) {
                            dialogInterface.dismiss();
                        }
                    }
                });
                builder.show();

            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name,email;
        CardView cardView;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.nameTv);
            email = (TextView) itemView.findViewById(R.id.emailTv);
            cardView = (CardView) itemView.findViewById(R.id.card);
        }
    }
}

