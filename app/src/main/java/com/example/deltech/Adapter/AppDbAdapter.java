package com.example.deltech.Adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.deltech.Pojo.DataPojo;
import com.example.deltech.Util.Util;

import java.util.ArrayList;
import java.util.List;

public class AppDbAdapter extends SQLiteOpenHelper
{
    public AppDbAdapter(Context context) {
        super(context, Util.app_database, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("create table student(name text , email text )");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void saveData(String Name , String Email )
    {
        SQLiteDatabase db=this.getWritableDatabase();

        ContentValues CV=new ContentValues();

        CV.put("name" , Name);
        CV.put("email" , Email);

        db.insert(Util.student , null , CV);

    }


    public Cursor showData()
    {
        Cursor c=null;

       SQLiteDatabase db =this.getReadableDatabase();

       c=db.rawQuery("select * from student" , null);

       if (c!=null)
       {
           c.moveToFirst();
       }


        return c;
    }
    public List<DataPojo> getAllContacts() {

        List<DataPojo> contactList = new ArrayList<DataPojo>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + "student";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                DataPojo contact = new DataPojo();
                contact.setName(cursor.getString(0));
                contact.setEmail(cursor.getString(1));
                contactList.add(contact);
            } while (cursor.moveToNext());
        }
        return contactList;
    }
    public void deleteData(String Email)
    {
        SQLiteDatabase db=this.getWritableDatabase();

        db.delete("student" , "email"+"=?" , new String[]{Email});
    }

    public void updateData(String Name , String Email, String previousEmail)
    {
        SQLiteDatabase db=this.getWritableDatabase();

        ContentValues CV=new ContentValues();

        CV.put("name" , Name);
        CV.put("email" , Email);


        db.update("student" , CV, "email"+"=?" , new String[]{previousEmail});
    }

}
