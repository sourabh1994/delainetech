package com.example.deltech.Fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.deltech.Activity.MainActivity;
import com.example.deltech.Adapter.AppDbAdapter;
import com.example.deltech.R;
import com.example.deltech.Util.Util;

public class SecondFrag extends Fragment {
    View rootView;
    EditText emailEt,nameEt;
    Button submitBtn;
    AppDbAdapter appDbAdapter;
    Bundle bundle;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView= inflater.inflate(R.layout.fragment_second, container, false);
        emailEt=rootView.findViewById(R.id.emailEt);
        nameEt=rootView.findViewById(R.id.nameEt);
        submitBtn=rootView.findViewById(R.id.submitBtn);
        appDbAdapter=new AppDbAdapter(getActivity());
        bundle=getArguments();
        if(bundle.getString("button").equals("update")){
            emailEt.setText(bundle.getString("email"));
            nameEt.setText(bundle.getString("name"));
        }
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String name=nameEt.getText().toString().trim();
                String email=emailEt.getText().toString().trim();
                if(!Util.isValidEmail(email))
                    Toast.makeText(getActivity(), "Please enter valid email address", Toast.LENGTH_SHORT).show();
                else if(name.equals(""))
                    Toast.makeText(getActivity(), "Please enter name", Toast.LENGTH_SHORT).show();
                else {
                    if (bundle.getString("button").equals("add")) {
                        appDbAdapter.saveData(name, email);
                        Toast.makeText(getActivity(), "Record Saved Successfully", Toast.LENGTH_LONG).show();
                    }
                    else {
                        appDbAdapter.updateData(name, email, bundle.getString("email"));
                        Toast.makeText(getActivity(), "Record updated Successfully", Toast.LENGTH_LONG).show();
                    }
                    ((MainActivity)getActivity()).onBackPressed();
                }

            }
        });
        return rootView;
    }
}
