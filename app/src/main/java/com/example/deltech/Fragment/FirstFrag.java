package com.example.deltech.Fragment;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.deltech.Activity.MainActivity;
import com.example.deltech.Adapter.AppDbAdapter;
import com.example.deltech.Adapter.DataAdapter;
import com.example.deltech.Interface.DataListenerInterface;
import com.example.deltech.Pojo.DataPojo;
import com.example.deltech.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;


public class FirstFrag extends Fragment implements DataListenerInterface, SwipeRefreshLayout.OnRefreshListener {
    View rootView;
    RecyclerView recyclerView;
    FloatingActionButton floatingActionButton;
    DataAdapter dataAdapter;
    private List<DataPojo> dataPojos = new ArrayList<>();
    AppDbAdapter appDbAdapter;
    SwipeRefreshLayout swipeRefreshLayout;
    public static DataListenerInterface dataListenerInterface;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView= inflater.inflate(R.layout.fragment_first, container, false);
        recyclerView=rootView.findViewById(R.id.recyclerView);
        floatingActionButton=rootView.findViewById(R.id.fab);
        swipeRefreshLayout=rootView.findViewById(R.id.swipeRefresh);
        dataListenerInterface= this;
        appDbAdapter=new AppDbAdapter(getActivity());
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(manager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(dataAdapter);
        dataAdapter=new DataAdapter(dataPojos,getActivity(),dataListenerInterface);
        recyclerView.setAdapter(dataAdapter);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        swipeRefreshLayout.postDelayed(new Runnable() {

            @Override
            public void run() {
                showData();
            }

        }, 100);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle =new Bundle();
                bundle.putString("button","add");
                SecondFrag secondFrag=new SecondFrag();
                secondFrag.setArguments(bundle);
                ((MainActivity) getActivity()).switchFragment(secondFrag, 1, "secondFrag");
            }
        });
        return rootView;
    }
    public  void showData(){
        Cursor cursor=appDbAdapter.showData();
        if(cursor.getCount()!=0){
            dataPojos.clear();
            swipeRefreshLayout.setRefreshing(true);
            if (cursor.moveToFirst()) {
                do {
                    DataPojo dataPojo = new DataPojo();
                    dataPojo.setName(cursor.getString(0));
                    dataPojo.setEmail(cursor.getString(1));
                    dataPojos.add(dataPojo);
                } while (cursor.moveToNext());}
        }

        swipeRefreshLayout.setRefreshing(false);
            dataAdapter.notifyDataSetChanged();

    }

    @Override
    public void data(Boolean refresh) {
        if(refresh)
            showData();
    }

    @Override
    public void onRefresh() {
        showData();
    }
}
